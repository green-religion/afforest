Bruce Peninsula:
maple, beech, oak: 
heapticas, trilliums, trout lily, dog-toothviolet, 
Dutchman’s breeches, Indian cucumber root, Jack in the pulpit, wild leeks, 
blue cohosh, and wild ginger.

jack pine, spruce, cedar:
fringed polygala, starflower, large-leaved aster, and 
orchidssuch as ram’s head and calypso, the dwarf lake iris.

Carolinian:
ash, birch, chestnut, hickory, oak, and walnut;
pawpaw
