/*
  // okay what do I need to do?
  // have a list of the species which are available
  // I want to output the density of planting based on their width, 
  // if no width mentioned it is assumed to be same as height. 
  // width/2 spacing is the recommended spacing for closed canopy
  // width spacing is for open canopy, 
  // which may be recommended in some instances
  //
  // based on the height can categorize them as canopy layer, 
  // tree layer, shrub layer, or ground-cover layer.
  // vine layer, tuber layer, are also possible. 
  // ideally we want at least 4 species per layer.
  //
  // if no species list available simply do it based on the input json
*/
const mysql = require('mysql');
const express = require('express');
const app = express();
const port = 80;
const fs = require('fs');
const heavy_clay = false;
const acid = false;
const available = false;

const golden = 1.618;

const land_offset = 5;
const land_length = 100;
const land_width = 100;
//const max_height = 40;
//const crown_overlap = 0;//1-1/golden;
const emergent_height = [30,100]; /* >=, < meters */
const canopy_height = [15,30]; /* >=, < meters */
const tree_height = [5,15]; /* =>, < meters */
const shrub_height = [1.5,5]; /* =>, < meters */
const herb_height = [0.5,1.5]; /* =>, < meters */
const ground_cover_height = [0, 0.5]; /* =>, < meters */
const hardyness = 4;

const my_plant_list = [
  { "name": "Prunus tenella", "quant": 20 }, 
  { "name": "Hippophae rhamnoides", "quant": 10}, 
  { "name": "Helianthus tuberosus", "quant": 100}
];

function my_plant_filter(plant_list, my_plant_list) {
  let plant_names = my_plant_list.map((plant) => {
    return plant.name;
  });
  console.log(plant_names);
  let filtered_plant_list = plant_list.filter((plant) => {
    console.log(plant["Latin Name"]);
    let my_match = plant_names.filter((name) => {
      return compare_plant_latin_name(plant, name) == true? true: false;
    });
    return my_match.length > 0? true: false;
  });
  return filtered_plant_list;
}

const input_json = {
  Hardyness: hardyness,
  Rating: 0,
  Full_Shade_Rating: 1,
  Heavy_Clay_Rating: 3,
  Semi_Shade_Rating: 2,
  No_Shade_Rating: 3,
  Nitrogen_Rating: 0,
  length: land_length/*m*/,
  width: land_width/*m*/,
  resolution: 100, // cm
  pH: "%N%", 
  Moisture: "%D%", /* M moist, D dry */
  Soil: "%S%", /* S sandy, H heavy, L loam */
  available: [
    /* ebay */ 
    "Ilex glabra",
    "Berberis gagnepainii", "Yucca filamentosa", "Gleditsia triacanthos", 
    "Ruta graveolens", "Ligustrum obtusifolium", "Buxus sempervirens", 
    "Lonicera japonica", "Lonicera ciliosa",  "Juniperus communis", 
    "Alnus viridis crispa", "Cytisus scoparius", "Myrica gale",
    "Wisteria floribunda", "Maackia amurensis", 
    /* local */
    "Elaeagnus umbellata", "Trifolium repens", "Melilotus albus", 
    "Scilla siberica",  "Galanthus nivalis", "Asarum canadense", 
    "Fagopyrum esculentum", 
    /* incredibleseeds.ca */ 

"Acer griseum", "Acer saccharum", "Albizia julibrissin", "Allium fistulosum", "Allium porum", "Amelanchier alnifolia", "Amorpha fruticosa", "Anethum graveolens", "Antirrhinum majus", "Apium graveolens", "Aronia melanocarpa", "Artemesia annua", "Artemisia absinthium", "Asclepias tuberosa", "Asimina triloba", "Asparagus officinalis", "Atriplex hortensis", "Basella rubra", "Belamcanda chinensis", "Borago officinalis", "Brassica oleracea", "Brassica rapa", "Calendula officinalis", "Callistephus chinensis", "Calycanthus floridus", "Campsis radicans", "Capsicum annum", "Capsicum annuum", "Caragana aborescens", "Carum carvi", "Carya ilinoinensis", "Carya ovata", "Castanea dentata", "Castanea mollissima", "Catalpa speciosa", "Chaenomeles japonica", "Chamaemelum nobile", "Chenopodium capitatum", "Citrullus lanatus", "Consolida ajacis", "Coriandrum sativum", "Cornus florida", "Cornus mas", "Corylus americana", "Corylus cornuta", "Cucumis melo", "Cucurbita maxima", "Cucurbita mixta", "Cucurbita moschata", "Cucurbita pepo", "Cuminum cyminum", "Cynoglossum amabile", "Delphinium belladonna", "Dianthus caryophyllus", "Diospyros virginiana", "Echinacea purpurea", "Exochorda racemosa", "Fragaria vesca", "Ginkgo biloba", "Glycine max", "Gymnocladus dioicus", "Gypsophila elegans", "Helianthus annus", "Helianthus annuus", "Helichrysum bracteatum", "Hibiscus syriacus", "Hippophae rhamnoides", "Hyssopus officinalis", "Ilex opaca", "Ipomoea tricolor", "Juglans cinerea", "Kolkwitzia amabilis", "Lactuca sativa", "Lagenaria siceraria", "Levisiticum officinalis", "Limonium sinuatum", "Linaria maroccana", "Liriodendron tulipifera", "Lycium chinensis", "Maclura pomifera", "Magnolia acuminata", "Magnolia liliflora", "Magnolia sieboldii", "Magnolia tripetala", "Mahonia aquifolium", "Matricaria chamomilla", "Matthiola incana", "Melissa officinalis", "Metasequoia glyptostroboides", "Monarda citriodora", "Monarda fistulosa", "Morus nigra", "Nepeta cataria", "Nepeta mussinii", "Nicotiana rustica", "Nigella damascena", "Nigella papillosa", "Ocimum basilicum", "Orlaya grandiflora", "Paeonia lactiflora", "Paeonia suffruticosa", "Paulownia tomentosa", "Petroselinum crispum", "Petunia multiflora", "Phacelia campanularia", "Phaseolus vulgaris", "Physalis philadelphica", "Physocarpus opulifolius", "Pinus koraiensis", "Pisum sativum", "Plantago major", "Prunus americana", "Prunus mahaleb", "Pycnanthemum pilosum", "Pyrus betulifolia", "Pyrus pyrifolia", "Pyrus ussuriensis", "Raphadus sativus", "Raphanus sativus", "Rheum rhabarbarum", "Rhus typhina", "Ribes nigrum", "Ribes sativum", "Robina pseudoacacia", "Robinia pseudoacacia", "Rosa canina", "Rubus idaeus", "Scabiosa atropurpurea", "Schisandra chinensis", "Shepherdia canadensis", "Silybum marianum", "Solanum burbankii", "Solanum lycopersicum", "Solanum melanocerasum", "Solanum melongena", "Styrax japonica", "Tagetes erecta", "Tagetes tenuifolia", "Thuja occidentalis", "Tithonia rotundifolia", "Trachymene coerulea", "Tropaeolum majus", "Vaccinium angustifolium", "Vaccinium macrocarpon", "Valerianella locusta", "Viburnum trilobum", "Vigna unguiculata", "Viola tricolor", "Vitis vinifera", "Wisteria floribunda", "Wisteria sinensis", "Zea mays", "Zinnia eleganz", "Zinnia haageana",
    /* bambooplants.ca */
"Abelia mosanensis", "Abelia x grandiflora", "Achillea millefolium", "Acorus calamus", "Acorus gramineus", "Actinidia arguta", "Actinidia kolomikta", "Alchemilla mollis", "Amelanchier alnifolia", "Andropogon gerardii", "Anthoxanthum nitens", "Aronia melanocarpa", "Arrhenatherum elatius bulbosum", "Artemisia schmidtiana", "Astilbe x arendsii", "Astilbe x rosea", "Berberis thunbergii", "Bergenia cordifolia", "Bouteloua curtipendula", "Bouteloua gracilis", "Buddleja davidii", "Buxus x", "Calamagrostis acutiflora", "Calamagrostis brachytricha", "Calamintha nepeta", "Callicarpa albibacca", "Callicarpa albibacca.", "Callicarpa bodinieri", "Callicarpa dichotoma", "Campanula pyramidalis", "Campsis radicans", "Campsis x tagliabuana", "Carex flacca", "Carex laxiculmis", "Carex oshimensis", "Caryopteris × clandonensis", "Caryopteris x clandonensis", "Cerastium tomentosum", "Ceratostigma plumbaginoides", "Chaenomeles superba", "Chaenomeles x superba", "Chasmanthium latifolium", "Clematis", "Clematis fargesioides", "Clematis tangutica", "Clematis tangutica.", "Coreopsis grandiflora", "Coreopsis rosea", "Cornus alba", "Cornus flaviramea", "Cornus flaviramea.", "Cornus sericea", "Cotoneaster acutifolius", "Cotoneaster dammeri", "Cotoneaster dammeri.", "Delosperma", "Deschampsia cespitosa", "Deutzia crenata", "Deutzia gracilis", "Deutzia gracilis.", "Deutzia x scabra", "Dianthus", "Dianthus deltoides", "Diervilla lonicera", "Diervilla lonicera.", "Echinacea purpurea", "Echium amoenum", "Euonymus alatus", "Euonymus alatus.", "Euonymus fortunei", "Euonymus fortunei.", "Euphorbia polychroma", "Fargesia dracocephala", "Fargesia sp.", "Festuca gautieri", "Festuca glauca", "Festuca idahoensis", "Festuca ovina", "Forsythia x intermedia", "Forsythia x intermedia.", "Fragaria", "Fragaria x", "Fuchsia magellanica", "Galium odorata", "Gaura linderheimeri", "Geranium", "Geranium phaeum", "Geranium Sanguineum", "Hakonechloa macra", "Hedera algeriensis", "Hedera helix", "Hedera pastuchovii", "Helenium bigelovii", "Helictotrichon sempervirens", "Helictotrichon sempervirens.", "Heliopsis helianthoides", "Hemerocallis", "Hibiscus moscheutos", "Hibiscus syriacus", "Hosta", "Humulus lupulus", "Hydrangea arborescens", "Hydrangea macrophylla", "Hydrangea paniculata", "Hydrangea paniculata.", "Hypercicum kalmianum", "Hypericum x inodorum", "Hyssopus officinalis", "Iberis sempervirens", "Imperata cylindrica", "Iris sibirica", "Kerria japonica", "Kniphofia", "Kolkwitzia amabilis", "Lamium maculatum", "Laurus nobilis", "Leucanthemum superbum", "Leucanthemum x superbum", "Leycesteria formosa", "Leymus arenarius (Elymus arenarius)", "Leymus arenarius (Elymus arenarius).", "Liatris spicata", "Ligustrum ovalifolium", "Ligustrum vicaryi.", "Ligustrum vulgare", "Ligustrum x vicaryi", "Lobelia siphilitica", "Lonicera × brownii", "Lonicera caerulea", "Lonicera tatarica.", "Lonicera xylosteoides", "Lycium barbarum", "Lycium ruthenicum", "Miscanthus sinensis", "Miscanthus x giganteus", "Molinia arundinacea", "Monarda", "Muhlenbergia capillaris", "Natural Nettles", "Nepeta nervosa", "Nepeta racemosa", "Pachysandra terminalis", "Panicum virgatum", "Parthenocissus quinquefolia", "Parthenocissus quinquefolia.", "Parthenocissus tricuspidata", "Pennisetum alopecuroides", "Pennisetum alopecuroïdes", "Pennisetum messiacum", "Pennisetum orientale", "Perovskia atriplicifolia", "Perovskia atriplicifolia.", "Phalaris arundinacea", "Philadelphus coronarius", "Philadelphus x virginalis", "Phlox subulata", "Phyllostachys bissetii", "Physocarpus opulifolius", "Picea pungens", "Platycodon", "Pleioblastus fortunei", "Pleioblastus pygmaeus", "Potentilla fruticosa", "Prunus cistena", "Prunus tomentosa", "Prunus virginiana", "Prunus x kerrasis", "Rhus aromatica", "Ribes alpinum", "Ribes alpinum.", "Ribes nigrum", "Ribes nigrum.", "Ribes rubrum", "Ribes uva", "Rubus fruticosus", "Rubus idaeus", "Rubus occidentalis", "Rudbeckia fulgida", "Saccharum ravennae", "Salix chaenomeloides", "Salix integra", "Salix purpurea", "Salix viminalis", "Salix viminalis.", "Sambucus canadensis", "Sambucus canadensis.", "Sasa palmata", "Schizachyrium scoparium", "Schizachyrium scoparium.", "Sedum", "Sedum sieboldii", "Sempervivum", "Sorbaria sorbifolia", "Sorbaria sorbifolia.", "Sorghastrum nutans", "Sorghastrum nutans.", "Spartina pectinata", "Spartina pectinata.", "Spiraea bumalda", "Spiraea japonica", "Spiraea nipponica", "Spiraea x arguta", "Spiraea x arguta.", "Spiraea x bumalda", "Spiraea x vanhouttei", "Spiraea x vanhouttei.", "Sporobolus heterolepis", "Sporobolus heterolepis.", "Symphoricarpos albus", "Symphoricarpos albus.", "Symphoricarpos x chenaultii", "Symphoricarpos x doorenbosii", "Syringa meyeri", "Syringa x prestoniae", "Urtica dioica", "Vaccinium corymbosum", "Verbascum phoeniceum", "Viburnum opulus", "Viburnum plicatum", "Viburnum trilobum", "Vitis", "Vitis riparia", "Vitis riparia.", "Weigela florida", "Weigelia florida", "Weigleia florida",
    /* Nortonnaturals.com */
    "Apios Americana", 
    "Helianthus tuberosa", 
    "Sagittaria latifolia",
    "Camassia quamash", 
    "Stachys affinis",
    "Hemerocallis fulva",
    "Amphicarpa bracteata", 
    "Allium canadense", 
    "Pediomelum esculentum",
    "Allium tricoccum", 
    "Claytonia virginica",
    "Lillium lancifolium",
    /* grimonut.ca stock */
    "Diospyros virginiana", "Fagus grandifolia",  "Juglans nigra",
    "Juglans x Buartnut", "Juglans cinerea", "Castanea mollissima",
    "Castanea dentata", "Ginkgo biloba", "Corylus heterophylla",
    "Juglans ailanthifolia",  "Carya ovata", "Carya laciniosa", 
    "Juglans cineria", "Rubus occidentalis", "Morus alba", "Morus Nigra",
    "Quercus macrocarpa", "quercus bicolor", "Carya illinoinensis",
    "Pinus koraiensis",  "Cydonia oblonga", "Juglans regia", 
    /* treetime.ca stock */
"abies balsamea", "abies balsamea phanerolepis", "abies concolor", "abies grandis", "abies lasiocarpa", "abies sibirica", "acer freemanii", "acer ginnala", "acer glabrum", "acer grandidentatum", "acer negundo", "acer platanoides", "acer pseudosieboldianum", "acer rubrum", "acer saccharinum", "acer saccharum", "acer spicatum", "acer tataricum", "aesculus glabra", "aesculus hippocastanum", "alnus crispa", "alnus incana", "amelanchier alnifolia", "arctostaphylos uva-ursi", "aronia melanocarpa", "beckmannia syzigachne", "berberis thunbergii", "betula", "betula alleghaniensis", "betula davurica", "betula nigra", "betula occidentalis", "betula papyrifera", "betula pendula", "betula pendula laciniata", "betula platyphylla fargo", "betula pumila", "buxus microphylla", "caragana arborescens", "caragana pygmaea", "carex aquatilis", "carpinus caroliniana", "carya ovata", "celastrus scandens", "celtis occidentalis", "cephalanthus occidentalis", "clematis occidentalis", "clematis virginiana", "cornus alba argenteo-marginata", "cornus racemosa", "cornus sericea", "cornus sericea flaviramea", "corylus americana", "corylus cornuta", "corylus heterophylla", "cotinus coggygria", "cotoneaster acutifolia", "crataegus arnoldiana", "crataegus cyclophylla", "crataegus douglasii", "crataegus phaenopyrum", "crataegus rotundifolia", "dasiphora fruticosa syn.", "echinacea angustifolia", "elaeagnus angustifolia", "elaeagnus commutata", "euonymus alatus", "fraxinus americana", "fraxinus mandschurica", "fraxinus pennsylvanica", "ginkgo biloba", "glyceria grandis", "hippophae rhamnoides l.", "hydrangea arborescens", "hydrangea paniculata", "ilex verticillata", "juglans cinerea", "juglans mandshurica", "juglans nigra", "juniperus communis", "juniperus horizontalis", "juniperus sabina", "juniperus virginiana", "larix laricina", "larix sibirica", "ledum groenlandicum", "liriodendron tulipifera", "lonicera caerulea", "lonicera dioica", "lonicera involucrata", "lycium barbarum", "maackia amurensis", "malus", "malus astringens", "malus baccata", "malus domestica", "malus pumila", "morus alba tatarica", "ostrya virginiana", "parthenocissus quinquefolia", "parthenocissus tricuspidata", "philadelphus coronarius", "physocarpus opulifolius", "picea abies", "picea engelmannii", "picea glauca", "picea glauca conica", "picea glauca", "picea glauca var. densata", "picea mariana", "picea meyeri", "picea omorika", "picea pungens", "picea rubens", "pinus albicaulis", "pinus aristata", "pinus banksiana", "pinus cembra", "pinus contorta", "pinus contorta var. latifolia", "pinus flexilis", "pinus koraiensis", "pinus mugo", "pinus nigra", "pinus ponderosa", "pinus resinosa", "pinus strobus", "pinus sylvestris", "platanus occidentalis", "populus", "populus balsamifera", "populus canadensis", "populus canescens", "populus deltoides", "populus deltoides jefcot", "populus jackii", "populus tremula", "populus tremuloides", "prinsepia sinensis", "prunus", "prunus armeniaca", "prunus armeniaca var. mandshurica", "prunus cerasus", "prunus cistena", "prunus fruticosa", "prunus kerrasis", "prunus maackii", "prunus nigra and americana", "prunus padus", "prunus pensylvanica", "prunus pumila var besseyi", "prunus serotina", "prunus tenella", "prunus tomentosa", "prunus virginiana", "prunus virginiana var. demissa", "pseudotsuga menziesii", "pyrus ussuriensis", "quercus alba", "quercus ellipsoidalis", "quercus macrocarpa", "quercus rubra", "rhododendron", "rhus glabra", "rhus typhina", "ribes alpinum", "ribes aureum", "ribes nigrum", "ribes oxyacanthoides", "ribes rubrum", "ribes triste", "robinia pseudoacacia", "rosa", "rosa acicularis", "rosa rugosa", "rosa s", "rosa woodsii", "rubus", "salix acutifolia", "salix alba vitellina", "salix amygdaloides", "salix bebbiana", "salix discolor or", "salix exigua", "salix integra", "salix pedicellaris", "salix pentandra", "sambucus canadensis", "sambucus racemosa", "scirpus pallidus", "shepherdia argentea", "shepherdia canadensis", "sorbus americana", "sorbus aucuparia", "sorbus decora", "sorbus scopulina", "spiraea japonica", "symphoricarpos albus", "symphoricarpos sp", "symphoricarpus occidentalis", "syringa", "syringa pekinensis", "syringa pubescens ss", "syringa reticulata", "syringa villosa", "syringa vulgaris", "thuja occidentalis", "tilia americana", "tilia cordata", "tilia flavescens", "tilia platyphyllos", "typha latifolia", "ulmus americana", "ulmus pumila", "vaccinium", "vaccinium corymbosum", "vaccinium myrtilloides", "vaccinium vitis-idaea", "viburnum dentatum", "viburnum edule", "viburnum lantana", "viburnum lentago", "viburnum opulus roseum", "viburnum trilobum", "vitis", "weigela florida", "wisteria macrostachya",
  ]
};

const strict = "true";

function derive_depth(plant) {
  //return height; //height/Math.tan(0.4);
  // derive depth based on row
  // emergent_height
  // canopy_height
  // tree_height
  // shrub_height
  // herb_height
  // ground_cover_height
  const path = 0.3;
  const widepath = 0.6;
  const ground_cover_width = 1;
  const herb_width = 2;
  const shrub_width = 4;
  const tree_width = 8;
  const canopy_width = 16;
  const emergent_width = 16;
  const height = plant.Height;
  const depth = height;
  const radius = derive_radius(plant);
  const seedling_radius = 0.3;
  let min_depth = path;
  if (depth - radius < min_depth) { 
    return min_depth + radius;
  }
  min_depth += ground_cover_width + path;
  let max_depth = min_depth + ground_cover_width;
  if (depth < max_depth) {
      return radius + depth < max_depth? depth: max_depth - radius;
  }
  min_depth += ground_cover_width + path;
  if (depth - radius < min_depth) { 
    return min_depth + radius;
  }
  max_depth = min_depth + herb_width;
  if (depth < max_depth) {
      return radius + depth < max_depth? depth: max_depth - radius;
  }
  min_depth += herb_width + path;
  if (depth - radius < min_depth) { 
    return min_depth + radius;
  }
  max_depth = min_depth + shrub_width;
  if (depth < max_depth) {
      return radius + depth < max_depth? depth: max_depth - radius;
  }
  min_depth += shrub_width + path;
  // this is for trees, they are over 3 meters high so radius can overlap;
  if (depth < min_depth) { 
    return min_depth + seedling_radius;
  }
  max_depth += tree_width;
  if (depth < max_depth) {
      return seedling_radius + depth < max_depth? depth: max_depth - 
      seedling_radius;
  }
  min_depth += tree_width + widepath;
  if (depth < min_depth) { 
    return min_depth + seedling_radius;
  }
  max_depth = min_depth + canopy_width;
  if (depth < max_depth) {
      return seedling_radius + depth < max_depth? depth: max_depth - 
      seedling_radius;
  }
  min_depth += canopy_width + widepath;
  if (depth < min_depth) { 
    return min_depth + seedling_radius;
  }
  max_depth = min_depth + emergent_width;
  return depth;


}

function compare_plant_latin_name(plant, name) {
      if (strict && plant['Latin Name'].toLowerCase() ==  name.toLowerCase()) {
        return true;
      }
      if (!strict && plant['Latin Name'].toLowerCase().substring(0,name.length) 
        == name.toLowerCase()) {
          return true;
      }
      return false;
}

function check_available(config, plant) {
  return config.available.filter((name) => {
    //const rexp = new RegExp(name.toLowerCase());
      return compare_plant_latin_name(plant, name);
      });

}
function derive_radius(plant) {
     return plant.Width != 0? plant.Width/2: plant.Height/3; 
}

function emergent_list(config, plants) {
  const emergent_plants = plants.filter((plant) => {
    if (plant.Height >= emergent_height[0] && plant.Height < emergent_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return emergent_plants;
}
function canopy_list(config, plants) {
  const canopy_plants = plants.filter((plant) => {
    if (plant.Height >= canopy_height[0] && plant.Height < canopy_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return canopy_plants;
}
function tree_list(config, plants) {
  const tree_plants = plants.filter((plant) => {
    if (plant.Height >= tree_height[0] && plant.Height < tree_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return tree_plants;
}
function shrub_list(config, plants) {
  const shrub_plants = plants.filter((plant) => {
    if (plant.Height >= shrub_height[0] && plant.Height < shrub_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return shrub_plants;
}
function herb_list(config, plants) {
  const herb_plants = plants.filter((plant) => {
    if (plant.Height >= herb_height[0] && plant.Height < herb_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return herb_plants;
}
function ground_cover_list(config, plants) {
  const ground_cover_plants = plants.filter((plant) => {
    if (plant.Height >= ground_cover_height[0] && plant.Height < ground_cover_height[1]) {
      const match = check_available(config, plant);
      if (match.length > 0) {
        return true;
      }
    }
    return false;
  });
  return ground_cover_plants;
}
function hedge_list(config, plants) {
  const ground_cover_plants = plants.filter((plant) => {
    if (plant.Habitat.search(/hedge/i) > -1) {
        return true;
    }
    return false;
  });
  return ground_cover_plants;
}

const connection = mysql.createConnection({
  host: "db",
  user: process.env.DB_USER,
  port: 3306,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE
});

function shade_filter(config, plant_list, shade, not_shade) {
  return plant_list.filter((plant) => {
      if (plant.Shade.indexOf(shade) > -1) {
        if (not_shade.length > 0) {
          if (plant.Shade.indexOf(not_shade[0]) > -1) {
            return false;
          }
          if (not_shade.length > 1) {
            if (plant.Shade.indexOf(not_shade[1]) > -1) {
              return false;
            }
          }
        }
        if (plant["Nitrogen fixer"] == 1 &&
          plant.Rating >= config.Nitrogen_Rating) {
          return true;
        }
        if (plant["Heavy clay"] == 1 &&
          plant.Rating >= config.Heavy_Clay_Rating) {
          return true;
        }
        if (shade == 'F' && plant.Rating < config.Semi_Shade_Rating) {
          return false;
        }
        if (shade == 'S' && plant.Rating < config.Semi_Shade_Rating) {
          return false;
        }
        if (shade == 'N' && plant.Rating < config.No_Shade_Rating) {
          return false;
        }
        return true;
      }
      return false;
    });

}

function plant_list_toString(plant_list) {
  const sorted_plant_list = plant_list.sort((first, compare) => {
    const height_compare =  compare.Height - first.Height;
    return height_compare != 0? height_compare:
      compare.Rating - first.Rating;
  });
  return sorted_plant_list.reduce((total, plant) => {
      const common_name = JSON.stringify(plant["Common name"]);
      const latin_name = plant["Latin Name"];
      const radius = plant.Width != 0? plant.Width/2 : plant.Height/2;
      const height = plant.Height;
      const nitrogen = plant["Nitrogen fixer"] == 1? 'n' : '';
      return total +` ${latin_name} (${common_name}) R:${plant.Rating} h:${height}m r:${radius}m ${nitrogen}, ft:${plant["Flowering time"]}, sr:${plant["Seed Ripens"]}`;
  }, "");
}


function plant_to_BEV_SVG(plant, location) {
  // trunk 3.4% of width (1/golden^7)
  let this_location = {};
  this_location.x = location.x*input_json.resolution;
  this_location.y = location.y*input_json.resolution;
  const trunk_width_percentage = 0.034;
  // assuming in cm resolution
  let svg = "";
  // trunk first
  const plant_radius = derive_radius(plant);
  const width = Math.round(plant_radius * input_json.resolution);
  const trunk_width = Math.round(width * trunk_width_percentage);
  svg += `<!-- ${plant["Latin Name"]} (${plant["Common name"]}, ` +
    ` Rating: ${plant.Rating}) -->\n`;
  svg += `<ellipse cx="${this_location.x}" cy="${this_location.y}" ` +
    ` ry="${trunk_width}" ` +
    ` rx="${trunk_width}" ` +
    ` fill="brown" opacity="1.0" />\n`;
  // canopy is width
  svg += `<ellipse cx="${this_location.x}" cy="${this_location.y}" ` +
    ` ry="${width}" ` +
    ` rx="${width}" ` +
    ` fill="green" opacity="0.1" />\n`;
  svg += `<text x="${this_location.x}" y="${this_location.y}">${plant["Latin Name"]}` +
    `</text>`;
  return svg;
}



const page_height = input_json.height * input_json.resolution;
const page_width = input_json.width * input_json.resolution;
const svg_header = `<svg width="${page_width}" height="${page_height}" `+
    ` xmlns="http://www.w3.org/2000/svg">\n`;

//function print_BEV_SVG(plant_list) {
//  const page_height = input_json.height * input_json.resolution;
//  const page_width = input_json.width * input_json.resolution;
//  let svg = "";
//  const header = `<svg width="${page_width}" height="${page_height}" `+
//    ` xmlns="http://www.w3.org/2000/svg">\n`;
//  svg += header;
//  //console.log(JSON.stringify(plant_list));
//  const emergent = emergent_list(input_json, plant_list);
//  //console.log(JSON.stringify(emergent));
//  let prev_x = 0;
//  let prev_y = 0;
//  emergent.forEach((plant) => {
//    //console.log(JSON.stringify(plant));
//    const plant_radius = plant.Width != 0? plant.Width/2 * input_json.resolution: 
//      plant.Height/2 * input_json.resolution;
//    if (prev_x+plant_radius >= (input_json.width*input_json.resolution)) {
//      prev_x = 0;
//      prev_y+=plant_radius;
//    }
//    if (prev_y >= (input_json.height*input_json.resolution)) {
//      prev_y = 0;
//      prev_x = 0;
//    }
//    const location = {x: plant_radius+prev_x, y: plant_radius+prev_y};
//    prev_x+=plant_radius;
//    svg += plant_to_BEV_SVG(plant, location);
//  });
//  svg += "</svg>\n";
//  return svg;
//}
//
function nitrogen_filter(plant_list) {
  return plant_list.filter((plant) => {
    if (plant["Nitrogen fixer"] == 1) {
      return true;
    }
    return false;
  });
}
function plantPrint(plant_list) {
    let name_list = ""; 
    const full_shade =  shade_filter(input_json, plant_list, 'F', '');
    name_list += `${full_shade.length} full shade ${plant_list.name} plants: `;
    name_list += plant_list_toString(full_shade);
    const semi_shade =  shade_filter(input_json, plant_list, 'S', 'F');
    name_list += `\n${semi_shade.length} semi shade ${plant_list.name} plants: `;
    name_list += plant_list_toString(semi_shade);
    const no_shade =  shade_filter(input_json, plant_list, 'N', 'FS');
    name_list += `\n${no_shade.length} no shade ${plant_list.name} plants: `;
    name_list += plant_list_toString(no_shade);
    const nitrogen_fixing = nitrogen_filter(plant_list);
    name_list += `\n${nitrogen_fixing.length} nitrogen fixing ${plant_list.name} plants: `;
    name_list += plant_list_toString(nitrogen_fixing);
    const full_length = no_shade.length + semi_shade.length + full_shade.length;
    name_list += `\nthere are ${full_length} ${plant_list.name} plants`;
    //name_list += "\n" + JSON.stringify(plant_list[0]);
    return name_list;
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function plant_scad(plant, location) {
  const width = derive_radius(plant)*2;
  return `// ${plant["Latin Name"]}\n` +
      `tree(${plant.Height}, ${width}, ${location.x}, ${location.y});\n`;
}

let plant_print = plant_scad;

function derive_overlap_reduction(height) {
  return height < 5? 2: height > 10? -0.3: 0;
}

function fill_height_row(full_plant_list, height, row_length) {
  const plant_list = full_plant_list.filter((plant) => {
    return plant.Height == height? true: false;
  });
  const random_plant_list = shuffle(plant_list);
  // for each plant until row length is filled put in a pair of plants
  let row_fill = 0;
  let total = new Array();
  while (row_fill < row_length) {
     const random_index = Math.floor(Math.random()*random_plant_list.length);
     const plant = random_plant_list[random_index];
     const radius = derive_radius(plant);
     const overlap_reduction = derive_overlap_reduction(plant.Height);
     const spacing = (radius + radius*overlap_reduction)*2;
     //row_fill += radius*4 - crown_overlap*radius*4;
     row_fill += spacing*2;
     if (row_fill > row_length) break;
     total.push(plant);
     total.push(plant);// 💚 couple
  }
  return total;
}
function fill_depth_row(full_plant_list, depth, row_length) {
  const plant_list = full_plant_list.filter((plant) => {
    return derive_depth(plant) == depth? true: false;
  });
  const random_plant_list = shuffle(plant_list);
  // for each plant until row length is filled put in a pair of plants
  let row_fill = 0;
  let total = new Array();
  while (row_fill < row_length) {
     const random_index = Math.floor(Math.random()*random_plant_list.length);
     const plant = random_plant_list[random_index];
     const radius = derive_radius(plant);
     const overlap_reduction = derive_overlap_reduction(plant.Height);
     const spacing = (radius + radius*overlap_reduction)*2;
     //row_fill += radius*4 - crown_overlap*radius*4;
     row_fill += spacing*2;
     if (row_fill > row_length) break;
     total.push(plant);
     total.push(plant);// 💚 couple
  }
  return total;
}

function row_to_SCAD(row_plants, direction) {
  let row_fill = 0;
  return row_plants.reduce((total, plant, index) => {
    const depth = land_offset + derive_depth(plant);
    const radius = derive_radius(plant);
    const overlap_reduction = derive_overlap_reduction(plant.Height);
    //const this_crown_overlap = crown_overlap - overlap_reduction;
    const offset = plant.Height < 5 && index % 2 == 0? radius : 0;
    const spacing = (radius + radius*overlap_reduction);
    const location = direction == 'S'?
      {y: depth, x: spacing + row_fill + depth +offset}:
      direction == 'W'?
      {x: depth, y: spacing + row_fill + depth +offset}:
      direction == 'E'?
      {x: input_json.width-depth, y: spacing + row_fill + depth + offset}:
      direction == 'N'?
      {y: input_json.length-depth, x: spacing + row_fill + depth + offset}:
      {};

    if (location.x == undefined) {
      throw Error("no such direction");
    }
    row_fill += spacing*2;
    return total.concat(plant_print(plant, location));
  }, new String());
}




function depth_filter(total, plant) {
  const depth = derive_depth(plant);
  if (total.indexOf(depth) == -1) {
    return total.concat(depth);
  }
  return total;
}

function print_SCAD(result) {
  let produce = "";
  const filter_by_height = result.filter((plant) => {
    return plant.Height > 0? true: false;
  });
  const filter_by_available = available? filter_by_height.filter((plant) => {
     return check_available(input_json, plant).length > 0? true: false;
  }) : filter_by_height;
  const sorted_by_height = filter_by_available.sort((compare, plant) => {
    return compare.Height - plant.Height;
  });
  // get list of heights
  // make a random array for each height
  // for each row south, east/west, north select appropriate shade level plants,
    // south side, no shade filter list
  // no shade, semi-shade and full shade
  const config = input_json;
  const no_shade = shade_filter(config, sorted_by_height, 'N', ''); 
  const semi_shade = shade_filter(config, sorted_by_height, 'S', ''); 
  const full_shade = shade_filter(config, sorted_by_height, 'F', ''); 

  // on a per height basis can fill in rows. 
  const no_shade_heights = no_shade.reduce((total, plant) => {
    if (total.indexOf(plant.Height) == -1) {
      return total.concat(plant.Height);
    }
    return total;
  }, new Array());
  const semi_shade_heights = semi_shade.reduce((total, plant) => {
    if (total.indexOf(plant.Height) == -1) {
      return total.concat(plant.Height);
    }
    return total;
  }, new Array());
  const full_shade_heights = full_shade.concat(semi_shade).reduce((total, plant) => {
    if (total.indexOf(plant.Height) == -1) {
      return total.concat(plant.Height);
    }
    return total;
  }, new Array());

  const no_shade_depths = no_shade.reduce((total, plant) => {
    return depth_filter(total, plant);
  }, new Array());
  const semi_shade_depths = semi_shade.reduce((total, plant) => {
    return depth_filter(total, plant);
  }, new Array());
  const full_shade_depths = full_shade.reduce((total, plant) => {
    return depth_filter(total, plant);
  }, new Array());

  // produce = produce.concat(no_shade_heights.reduce((total, height) => {
  //   const depth = derive_depth(height); //Math.atan(45)*plant.Height;
  //   const row_length = input_json.length-land_offset*2-depth*2;
  //   const row_plants = fill_height_row(no_shade, height, row_length);
  //   return total.concat(row_to_SCAD(row_plants, 'S'));
  // }, new String()));
  // produce = produce.concat(semi_shade_heights.reduce((total, height) => {
  //   const depth = derive_depth(height); //Math.atan(45)*plant.Height;
  //   const row_length = input_json.length-land_offset*2-depth*2;
  //   const row_plants = fill_height_row(semi_shade, height, row_length);
  //   return total.concat(row_to_SCAD(row_plants, 'E')).concat(
  //                       row_to_SCAD(row_plants, 'W'));
  // }, new String()));
  // produce = produce.concat(full_shade_heights.reduce((total, height) => {
  //   const depth = derive_depth(height); //Math.atan(45)*plant.Height;
  //   const row_length = input_json.length-land_offset*2-depth*2;
  //   const row_plants = fill_height_row(full_shade.concat(semi_shade), 
  //     height, row_length);
  //   return total.concat(row_to_SCAD(row_plants, 'N'));
  //                       
  // }, new String()));
  produce = produce.concat(no_shade_depths.reduce((total, depth) => {
    const row_length = input_json.length-land_offset*2-depth*2;
    const row_plants = fill_depth_row(no_shade, depth, row_length);
    return total.concat(row_to_SCAD(row_plants, 'S'));
  }, new String()));
  produce = produce.concat(semi_shade_depths.reduce((total, depth) => {
    const row_length = input_json.length-land_offset*2-depth*2;
    const row_plants = fill_depth_row(semi_shade, depth, row_length);
    return total.concat(row_to_SCAD(row_plants, 'E')).concat(
                        row_to_SCAD(row_plants, 'W'));
  }, new String()));
  produce = produce.concat(full_shade_depths.reduce((total, depth) => {
    const row_length = input_json.length-land_offset*2-depth*2;
    const row_plants = fill_depth_row(full_shade.concat(semi_shade), 
      depth, row_length);
    return total.concat(row_to_SCAD(row_plants, 'N'));
                        
  }, new String()));
  

  //// can integrate latin name on trunk or something
  return produce;
}

// a new way of doing the tree planting
// have a grid, which can receive metadata. 
// each grid point can have a plant name if its trunk is there, 
  // and a list of plants which overlap that square if any
// tree-web (grid)
// should start with the large plants, then fill in with the smaller ones
// also start with middle can then spiral outwards. 
// function tree_web_plant(plant_list) {
//   const web_length = 300;
//   let tree_web = new Array(web_length);
//   tree_web.seal();
//   tree_web = tree_web.map(() => {
//     return new Array(web_length);
//   });
//   //const centre = web_length/2;
//   const sorted_plant_list = plant_list.sort((first, compare) => {
//     const height_compare =  compare.Height - first.Height;
//     return height_compare != 0? height_compare:
//       compare.Rating - first.Rating;
//   });
//   sorted_plant_list.reduce((total, plant) => {
//       let location = {x: derive_depth, y: derive_depth};
//       return total.concat(plant_print(plant, location)); }, 
//     new String());
// }
//
function my_plant_identify(plant, my_plant_list) {
  return my_plant_list.filter((my_plant) => {
    return compare_plant_latin_name(plant, my_plant.name); })[0];
}


connection.connect((err) => {
  if (err) throw err;
  console.log('Connected!');
  const headings = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS ' +
                   ' WHERE TABLE_NAME = N\'PlantsForAFuture\' ' +
                   ' ORDER BY ORDINAL_POSITION';
  const sql = 'select `Latin Name`, `Common name`, `Hardyness`, `Range`, ' +
    ' `Rating`, `Shade`, `Height`, `Width`, `Habit`, `pH`, ' + 
    ' `Moisture`, `Uses notes`, `Habitat`, `Soil`, `Well-drained`, ' +
    ' `Nitrogen fixer`, `Heavy clay`, `Acid`, `Seed Ripens`, `Flowering time` ' +
    ` from PlantsForAFuture where \n` +
    `Hardyness <=${input_json.Hardyness}\n `+
    `AND Hardyness > 0 ` +
    `AND Rating >=${input_json.Rating} ` +
    `AND Moisture like '${input_json.Moisture}' ` +
    `AND pH like '${input_json.pH}' `;
    //`AND Moisture `;
  const evergreen_sql = 
    'select `Latin Name`, `Common name`, `Hardyness`, `Range`, ' +
    ' `Rating`, `Shade`, `Height`, `Width`, `Habit`, `pH`, ' + 
    ' `Moisture`, `Uses notes`, `Habitat`,  `Deciduous_Evergreen`, ' +
    ' `Nitrogen fixer`, `Heavy clay`, `Acid`, `Seed Ripens`, `Flowering time` ' +
    ` from PlantsForAFuture where ` +
    `Hardyness <=${input_json.Hardyness} `+
    //`AND Hardyness > 0 ` +
    `AND Rating >=${input_json.Rating} ` +
    `AND Moisture like '${input_json.Moisture}' ` +
    `AND Soil like '${input_json.Soil}' ` +
    `AND pH like '${input_json.pH}' `;
  /* my plant list */
  connection.query(sql, function(err, raw_result) {
    if (err) throw err;
    const plant_list = my_plant_filter(raw_result, my_plant_list);
    console.log(plant_list);
    const result = plant_list.reduce((total, plant) => {
      let height = plant.Height;
      let radius = derive_radius(plant);
      let location = {"x": height, "y": height};
      let lines = `<!-- location ${JSON.stringify(location)}-->\n`;
      let line =  plant_to_BEV_SVG(plant, location).concat("\n")
      lines += line;
        lines += `<!-- radius1 ${radius} location ` +
          ` ${JSON.stringify(location)}-->\n`;
      let my_plant = my_plant_identify(plant, my_plant_list);
      lines += `<!-- ${JSON.stringify(my_plant)} name ${my_plant.name} quant ${my_plant.quant}-->\n`;
      for (let i = 0; i < my_plant.quant; i++ ) {
        location.x = location.x + radius*2;
        lines += `<!-- radius ${radius} location ` +
          ` ${JSON.stringify(location)}-->\n`;
        line = plant_to_BEV_SVG(plant, location).concat("\n");
        lines = lines.concat(line);
      }
      return total.concat(lines);
    }, new String());
    let intro = `<svg width="10000" height="10000"  `+
      `xmlns="http://www.w3.org/2000/svg">\n`;
    let outro = `</svg>\n`
    let produce = intro + result + outro;
    fs.writeFile("/app/data/myplants.svg", produce, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
  });
  connection.query(evergreen_sql, function (err, raw_result) {
    if (err) throw err;
    const result = raw_result.filter((plant) => {
      if (heavy_clay) {
      return plant["Heavy clay"] == 1? true: false;
      } 
      if (acid) {
      return plant["Acid"] == 1? true: false;
      } 
      return true;
    });
    const evergreen = result.filter((plant) => {
      return plant.Deciduous_Evergreen == "E"? true: false;
    });
    fs.writeFile("/app/data/evergreen.txt", JSON.stringify(evergreen), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
    fs.writeFile("/app/data/evergreen.scad", print_SCAD(evergreen), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
  });
  connection.query(evergreen_sql, function (err, raw_result) {
    if (err) throw err;
    const result = raw_result.filter((plant) => {
      if (heavy_clay) {
      return plant["Heavy clay"] == 1? true: false;
      } 
      if (acid) {
      return plant["Acid"] == 1? true: false;
      } 
      return true;
    });
    const nitrogen = result.filter((plant) => {
      return plant["Nitrogen fixer"] == 1? true: false;
    });
    fs.writeFile("/app/data/nitrogen.txt", JSON.stringify(nitrogen), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
    fs.writeFile("/app/data/nitrogen.scad", print_SCAD(nitrogen), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
  });
  connection.query(sql, function (err, raw_result) {
    if (err) throw err;
    const result = raw_result.filter((plant) => {
      if (heavy_clay) {
        return plant["Heavy clay"] == 1? true: false;
      } 
      if (acid) {
      return plant["Acid"] == 1? true: false;
      } 
      return true;
    });

    //console.log("Result: " + JSON.stringify(result));
    //console.log("There were " + result.length + " results");
    let emergent = emergent_list(input_json, result);
    emergent.name = 'emergent';
    console.log(plantPrint(emergent));
    let canopy = canopy_list(input_json, result);
    canopy.name = 'canopy';
    console.log(plantPrint(canopy));
    let tree = tree_list(input_json, result);
    tree.name = 'tree';
    console.log(plantPrint(tree));
    let shrub = shrub_list(input_json, result);
    shrub.name = 'shrub';
    console.log(plantPrint(shrub));
    let herb = herb_list(input_json, result);
    herb.name = 'herb';
    console.log(plantPrint(herb));
    let ground_cover = ground_cover_list(input_json, result);
    ground_cover.name = 'ground cover';
    console.log(plantPrint(ground_cover));
    let hedge = hedge_list(input_json, result);
    hedge.name = 'hedge';
    //console.log(plantPrint(hedge));
    //console.log(print_SCAD(result));
    fs.writeFile("/app/data/out.txt", JSON.stringify(result), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
    fs.writeFile("/app/data/out.scad", print_SCAD(result), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
    plant_print = plant_to_BEV_SVG;
    let svg = svg_header.concat(print_SCAD(result)).concat("</svg>");
    fs.writeFile("/app/data/out.svg", svg, function(err) {
      if(err) {
          return console.log(err);
      }
      console.log("The file was saved!");
    }); 
  });
  connection.query(headings, function (err, result) {
    if (err) throw err;
    const heading_names = result.reduce((total, object) => {
      return total + `${object.COLUMN_NAME}, `
    });
    console.log("Result: " + heading_names);
  });
  const count = 'select count(*) from PlantsForAFuture';
  connection.query(count, function (err, result) {
    if (err) throw err;
    console.log("there are: " + JSON.stringify(result) + " rows.");
  });

});

app.get('/', (req, res) =>  {
  const sql = 'select `Latin Name`, `Common name`, `Hardyness`, `Range`, ' +
    ' `Rating`, `Shade`, `Height`, `Width`, `Habit`, `pH`, ' + 
    ' `Moisture`, `Uses notes`, `Habitat`, `Deciduous_Evergreen` ' +
    ` from PlantsForAFuture where \n` +
    `Hardyness <=${input_json.Hardyness}\n `+
    `AND Hardyness > 0 ` +
    `AND Rating >=${input_json.Rating} ` +
    `AND Moisture like '${input_json.Moisture}' ` +
    `AND Soil like '${input_json.Soil}' ` +
    `AND pH like '${input_json.pH}' `;
  connection.query(sql, function (err, result) {
    if (err) throw err;
    plant_print = plant_to_BEV_SVG;
    let svg = svg_header.concat(print_SCAD(result)).concat("</svg>");
    //const filter_by_height = result.filter((plant) => {
    //  return plant.Height > 0? true: false;
    //});
    //console.log(JSON.stringify(filter_by_height[0]));
    res.send(svg);
  });
  // want to get it to print by height
});

app.use('/static', express.static('/app/OpenJSCAD.org/packages/web'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
//app.js

const mysqldump = require ('mysqldump');
// or const mysqldump = require('mysqldump')
 
// dump the result straight to a file
mysqldump({
    connection: {
  host: "db",
  user: process.env.DB_USER,
  port: 3306,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE
    },
    dumpToFile: './data/dump.sql',
});
