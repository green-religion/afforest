#!/bin/bash
. .env
docker build -f pfaf.docker -t liberit/pfafdb:latest . || exit 1
docker build -f pfafjs.docker -t liberit/pfafjs:latest . || exit 1
# docker-compose pull 
if [[ ! -d ./data ]]
then
  mkdir data
fi
docker-compose down
docker-compose up
