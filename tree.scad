//!OpenSCAD
// tree

// trunk
trunk_ratio = 0.034;
module trunk(height, radius) {
    color("Sienna", 1.0) {
    cylinder(h=height, r=radius);
    }
}
// canopy
module canopy(height, radius, canopy_height) {
    color("green", 0.1) {
        translate([0,0,height-canopy_height/2]) {
           resize([radius*2, radius*2, canopy_height]) {
            
               sphere(r=radius);
            }
        }
    }
}

function m_to_mm(m) = m*1000;

module tree(height_in_m, width_in_m, x_loc, y_loc) {
  translate ([m_to_mm(x_loc), m_to_mm(y_loc), 0]) {
  canopy_height = height_in_m < width_in_m? height_in_m:
      height_in_m < 5? width_in_m :
      width_in_m > height_in_m*golden? height_in_m*golden :
      height_in_m < 10? width_in_m :
      height_in_m < 31? 10: 20;
  trunk(m_to_mm(height_in_m), m_to_mm(width_in_m/2*trunk_ratio));
  //canopy(m_to_mm(height_in_m), m_to_mm(width_in_m/2), 
   // m_to_mm(canopy_height));
  }
}


/*
module line() {
tree(3,3,7,7);
tree(10,10,18.6,18.6);
tree(5,5,10.3,10.3);
tree(15,15,27,27);
tree(20,20,35,35);
tree(30,30,50,50);
}

module many_tree() {
  
}


many_tree(tree_array);
line();
mirror([0,0]) {
    line();
}
*/
 full_road_width = m_to_mm(3+1.3*2);
module road() {
    // 3m wide
    // 1.3m shoulder*2
   
    road_width = m_to_mm(3);
    translate([-full_road_width/2,0,0]){
      color("wheat") {
      cube([full_road_width, m_to_mm(100), 10], center=false);
      }
    }
    translate([-road_width/2,0,0]){
      color("lightgrey") {
      cube([road_width, m_to_mm(100), 20], center=false);
      }
    }
   
}
full_path_width = m_to_mm(1.3*2);
path_width = m_to_mm(3);

module path() {
    // 3m wide
    // 1.3m shoulder*2
    
    
    translate([0,-full_path_width/2,0]){
      color("wheat") {
      cube([m_to_mm(100), full_path_width,  10], center=false);
      }
    }
   
}

module swale (length, width, depth, offset) {

polyhedron (points=[[0,0,0],[0,length,0],[offset,length,-depth],
[offset,0,-depth],
    [width,0,0],[width,length,0]],
                     faces=[[0,1,2,3],[1,5,6,2],[0,4,7,3],[0,1,5,4],[5,2,3,4]]); 
    
}

swale_width = m_to_mm(3);
golden = 1.618;
module the_swale() {
  length = m_to_mm(100)/golden;
  width = swale_width;
  depth = m_to_mm(1.06);
  offset = m_to_mm(1.85);  
  color("cyan") {
  translate([full_road_width/2,swale_width*3,0]) {
  swale(length, width, depth, offset);
  }
  }
}


module green_field() {
    translate([full_road_width/2, swale_width,0]) {
    color("green",0.1) {
      cube(size=[m_to_mm(48*2),m_to_mm(48*2),100], center=false);
    }
    }   
}
module swale2() {
  translate([0,-full_road_width/2+path_width/2,0]) {
  mirror([1,0,0]) {
    rotate([0,0,90]) {
        the_swale();
      }
    }
  }
}

module tinyhome() {
    color("brown") {
    translate([m_to_mm(100-61), m_to_mm(61), 0]) {
    cube(size=[m_to_mm(2.5), m_to_mm(6), m_to_mm(3)], center=false);
    }}
}
module person() {
    color("blue") {
      translate([m_to_mm(100-61), m_to_mm(58), m_to_mm(1)]) {
    resize([m_to_mm(0.3), m_to_mm(0.7), m_to_mm(2)]){
    sphere(d=m_to_mm(2));
    }
}
}
}

module a_road(from, to, radius) {
  fx=from[0];
  fy=from[1];
  tx=to[0];
  ty=to[1];  
  polyhedron(points=
    [[fx+radius,fy-radius,0],[fx-radius,fy-radius,0],
    [tx-radius,ty+radius,0],[tx+radius,ty+radius,0],
    [fx+radius,fy+radius,10],[fx-radius,fy-radius,10],
    [tx+radius,ty+radius,10],[tx-radius,ty-radius,10]],
    faces=[[0,3,2,1],[4,5,6,7],[0,1,4,5],[0,4,6,2],[1,5,7,3]]);
}

function m2mm(m)=m*1000;
a_road([m2mm(6),m2mm(5)],[m2mm(6),m2mm(100-6)], m2mm(0.3));
a_road([m2mm(8),m2mm(5)],[m2mm(8),m2mm(100-6)], m2mm(0.3));
a_road([m2mm(12),m2mm(5)],[m2mm(12),m2mm(100-6)], m2mm(0.3));
a_road([m2mm(20),m2mm(5)],[m2mm(20),m2mm(100-6)], m2mm(0.6));
a_road([m2mm(36),m2mm(5)],[m2mm(36),m2mm(100-6)], m2mm(0.6));


a_road([m2mm(6),m2mm(5)],[m2mm(94),m2mm(5)], m2mm(0.3));
a_road([m2mm(6),m2mm(7)],[m2mm(94),m2mm(7)], m2mm(0.3));
a_road([m2mm(6),m2mm(11)],[m2mm(94),m2mm(11)], m2mm(0.3));
a_road([m2mm(6),m2mm(19)],[m2mm(94),m2mm(19)], m2mm(0.6));
a_road([m2mm(6),m2mm(35)],[m2mm(94),m2mm(35)], m2mm(0.6));
//a_road([m2mm(6),m2mm(11)],[m2mm(100),m2mm(11)], m2mm(0.3));
//a_road([m2mm(6),m2mm(16)],[m2mm(100),m2mm(16)], m2mm(0.6));


road();
path();
the_swale();
green_field();
swale2();

tinyhome();
person();

// add lines from out.scad after
