server.js has the configuration files, can check the comments and input_json for
configuration settings

prerequisites:
download PlantsForAFuture.sql (Temperate Plant Database) from 
https://pfaf.org/user/shop.aspx

install docker

type:
./compile.sh

check ./data folder for outgoing scad, txt and svg files

can add the contents of data/out.scad to tree.scad to see what the forest garden
plus property looks like. 
Unless you have an extremely fast computer it would be prudent to first open
tree.scad in OpenSCAD and get out to a zoom where you can see the whole property
easily. Then copy paste the contents of out.scad to the end in the editor. 

for example using
cat data/out.scad |xclip


Note you should double check the plants are suitable for your soil and hardyness
before buying, Plants for a future database is not infallible. You can also
update the sql file according to your preferences. Also some of the hardyness
set at 0 are not actually hardy in zone 0, but the pfaf people simply didn't
know the hardiness at time of entry, so they may need to be updated. 

Another example the server.js script be default ignores (non nitrogen fixers) with
edibility Rating less than 3. So you may want to either lower that, or raise the
edibility rating of desirable plants higher than 3. For example the humble
Inkberry (Ilex glabra) is the only caffeine leaf bearing shrub in hardyness
zones 3-7 but by default it's edibility is only 1. Can raise that to three for
this evergreen tea/coffee substitue to be part of your edible food forest 
garden. 

TODO:

There is still much to be done, for instance splitting it into rows, so can
actually walk between the plants and harvest. Also if your property is not
completely flat you may need to make some adjustments to how you distribute your
plants. Every site has it's own specific needs. This is for generic
recommendations only. No warranty. 
